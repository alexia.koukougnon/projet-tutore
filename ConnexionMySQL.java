import java.sql.*;

public class ConnexionMySQL {

    private Connection mysql;
    private boolean connecte = false;

    /** Charge le driver */
    public ConnexionMySQL() throws ClassNotFoundException{
        try{
            Class.forName("org.mariadb.jdbc.Driver");
        }
        catch(ClassNotFoundException e){
            System.out.println("Driver MySQL non trouvé");
            mysql = null;
            return;
        }
    }

    /** crée la connexion */
    public void connecter(String nomServeur, String nomBase, String nomLogin, String motDePasse) throws SQLException {
        try{
            mysql = DriverManager.getConnection("jdbc:mysql://" + nomServeur + ":3306/" + nomBase,nomLogin,motDePasse);
            connecte = true;
        }
        catch(SQLException e){
            System.out.println("Echec de connection");
            System.out.println(e.getMessage());
            mysql = null;
            return;
        }
    }

    /** ferme la connexion */
    public void close() throws SQLException {

    }

    public boolean isConnecte(){
        return this.connecte;
    }

    public Blob createBlob()throws SQLException{
        return this.mysql.createBlob();
    }

    public Statement createStatement() throws SQLException {
        return this.mysql.createStatement();
    }

    public PreparedStatement prepareStatement(String requete) throws SQLException{
        return this.mysql.prepareStatement(requete);
    }
}
