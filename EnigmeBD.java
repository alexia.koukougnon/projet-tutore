import java.sql.*;
import java.util.List;
import java.util.ArrayList;


public class EnigmeBD{

    private ConnexionMySQL laconnexion;

    public EnigmeBD(ConnexionMySQL laconnexion){
        this.laconnexion = laconnexion;
    }

    public List<Enigme> getEnigme() throws SQLException{
        Statement st = this.laconnexion.createStatement();
        ResultSet rs = st.executeQuery("select nomEn, brouillonEn from ENIGME");
        List<Enigme> res = new ArrayList<>();
        while (rs.next()){
            String nomen = rs.getString(1);
            boolean etat = rs.getString(2).charAt(0) == 'O'; // true si c'est un brouillon
            res.add(new Enigme(nomen, etat));
            rs.next();
        }
        return res;
    }

    public Enigme getEnigmeParNom(String nom) throws SQLException{
        Statement st = this.laconnexion.createStatement();
        ResultSet rs = st.executeQuery("select nomEn, brouillonEn from ENIGME where nomEn = " + nom);
        if (rs.next()){
            return new Enigme(rs.getString(1), rs.getString(2).charAt(0) == 'O');
        }
        else{
            throw new SQLException("Cet enigme n'existe pas.");
        }
    }

    public boolean insertEnigme(int iden, String nom, String texte, byte[] image, String reponse, String aide, boolean brouillon, int idUt) throws SQLException{
        PreparedStatement pst = this.laconnexion.prepareStatement("insert into JOUEUR values(?,?,?,?,?,?,?,?)");
        Statement st = this.laconnexion.createStatement();
        ResultSet rs = st.executeQuery("select IFNULL(max(idEn),0) lemax from ENIGME");
        rs.next();
        int res = rs.getInt("lemax");
        rs.close();
        pst.setInt(1, res+1);
        pst.setString(2, nom);
        pst.setString(3, texte);
        Blob b = this.laconnexion.createBlob();
        b.setBytes(1, image);
        pst.setBlob(4, b);
        pst.setString(5, reponse);
        pst.setString(6, aide);
        if (brouillon){
            pst.setString(7, "O");
        }
        else{
            pst.setString(7, "N");
        }
        pst.setInt(8, idUt);
        return true;
    }

    public void effacerEnigme(int num) throws SQLException {
        PreparedStatement pst = this.laConnexion.prepareStatement("delete from ENIGME where idEn = ?");
        pst.setInt(1, num);
        pst.executeUpdate();
    }

}