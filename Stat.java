import java.sql.*;

public class Stat{
    
    private byte[] avatar;
    private boolean actif;
    private int numPartie;
    private String scenario;
    private Date temps;
    private boolean fini;

    public Stat(byte[] avatar, boolean actif, int numPartie, String scenario, Date temps, boolean fini){
        this.avatar = avatar;
        this.actif = actif;
        this.numPartie = numPartie;
        this.scenario = scenario;
        this.temps = temps;
        this.fini = fini;
    }

    public byte[] getAvatar(){
        return this.avatar;
    }

    public boolean getActif(){
        return this.actif;
    }

    public int getNumPartie(){
        return this.numPartie;
    }

    public String getScenario(){
        return this.scenario;
    }

    public Date getTemps(){
        return this.temps;
    }

    public boolean getFini(){
        return this.fini;
    }

}